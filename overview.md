# Deploy SharePoint Add-in build/release step #

VSTS release/build step to deploy SharePoint add-ins to SharePoint tenants or farms.
This extension lets you deploy your add-ins to SharePoint directly from your release definitions.

## Deprecation ##
Warning: this extension has been deprecated and will be removed from the Market Place in the near future. We have made another extension that we encourage you to use instead. This extension allows you to deploy both SharePoint-hosted add-ins as well as Provider-hosted add-ins.

Get the alternative extension [here](https://marketplace.visualstudio.com/items?itemName=projectumapsvsts.upload-sp-provider-hosted-app).

# Release Notes
## 1.2.8 (25-Jul-2018)
- Added MarkDown - including deprecation message


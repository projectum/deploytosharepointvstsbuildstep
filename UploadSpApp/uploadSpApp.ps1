[CmdletBinding()]
param()

Trace-VstsEnteringInvocation $MyInvocation
try {
    # get task properties
    $paramUrl = Get-VstsInput -Name url -Require
	$paramLogin = Get-VstsInput -Name login -Require
	$paramPassword = Get-VstsInput -Name password -Require
	$paramFiles = Get-VstsInput -Name appFiles -Require

	# log properties to task output
    Write-Host "Site URL: $paramUrl"
	Write-Host "Login: $paramLogin"

	# load SharePoint CSOM assemblies
	Add-Type -Path Microsoft.SharePoint.Client.dll
	Add-Type -Path Microsoft.SharePoint.Client.Runtime.dll
	Add-Type -Path Microsoft.SharePoint.Client.UserProfiles.dll

	# prepare credentials to be used to connect to app catalog
	$securePassword = ConvertTo-SecureString $paramPassword -AsPlainText -Force 
	$creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($paramLogin, $securePassword)

	# init SP context
	$ctx = New-Object Microsoft.SharePoint.Client.ClientContext($paramUrl)
	$ctx.Credentials = $creds

	# load app catalog library and root folder
	$appsLibName = "Apps for SharePoint"
	$appsLib = $ctx.Web.Lists.GetByTitle($appsLibName)
	$ctx.Load($appsLib)
	$ctx.Load($appsLib.RootFolder)
	$ctx.ExecuteQuery()

	# handle files parameter
	if (!$paramFiles.Contains('.app')) {
		$paramFiles = Join-Path $paramFiles '/**/*.app'
	}

	$appFiles = Get-ChildItem -Path $paramFiles -Recurse

	# upload .app files
	Foreach ($appFile in $appFiles)
	{
		Write-Host "Uploading '$appFile'..."

		$fileStream = New-Object IO.FileStream($appFile.FullName,[System.IO.FileMode]::Open)
		$fileURL = $appsLib.RootFolder.ServerRelativeUrl + "/" + $appFile.Name

		$fileCreationInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
		$fileCreationInfo.Overwrite = $true
		$fileCreationInfo.ContentStream = $fileStream
		$fileCreationInfo.URL = $fileURL

		$addedFile = $appsLib.RootFolder.Files.Add($fileCreationInfo)
		$ctx.Load($addedFile)
		$ctx.ExecuteQuery()
	}

	Write-Host "Finished uploading .app files to '$paramUrl' App Catalog."
} finally {
    Trace-VstsLeavingInvocation $MyInvocation
}
